---
output: 
  pdf_document: 
    fig_height: 3.5
---

\includegraphics[height = 2.0cm]{logo/logo_cermel.png}

\begin{center}
\Large{\textbf{BASELINE FLORA}}
\end{center}

\vspace{0.5cm}

```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
options(knitr.kable.NA = "-")
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
library(readxl)
library(dplyr)
library(knitr)
library(stringr)
library(kableExtra)
library(tidyr)
library(lubridate)


FloraScreening <- read_excel("data/FloraDatabase.xlsx", sheet = "FloraScreening")

FloraAE <- read_excel("data/FloraDatabase.xlsx", sheet = "FloraAE") %>%
  select(-Remarks, -Remarks_2)%>%
  mutate(ndays = difftime(Occurence_Date, Enrolment_date, units = "days"),
         days = case_when(ndays <= 10 ~ "10 days after",
                          ndays <= 30 ~ "1 month after",
                          ndays <= 90 ~ "3 months after",
                          ndays > 90 ~ "6 months after"))

names(FloraScreening) <- c("screening_ID", "study_ID", "visit_day", "date_enrollment", "date_visit",
                           "date_result", "ascaris_egg", "trichuris_egg", "hookworm_egg",
                           "other_egg", "date_result2", "count", "Specie", "date_result3",
                           "hemoculture_result", "date_result4", "coproculture_result", "random",
                           "gender", "age", "height", "weight", "bmi", "body_tmp", "heart_rate",
                           "systol_diast")
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}

FLORA_data <- FloraScreening %>%
  filter(visit_day == "Screening") %>%
  separate(systol_diast, into = c("systolic", "diastolic"), sep = "/") %>%
  mutate_at(vars(c(systolic, diastolic, weight, count, heart_rate)), .funs = as.numeric)%>%
  mutate(co_infection = case_when(hookworm_egg != "0" & trichuris_egg != "0" & ascaris_egg != "0" ~ "hook/trichuris/ascaris",
        trichuris_egg != "0" & hookworm_egg != "0" &  Specie == "Strongyloids"~ "hook/trichuris/strongyloids",
                                  hookworm_egg != "0" & ascaris_egg != "0" ~ "hook/ascaris",
                                  hookworm_egg != "0" & trichuris_egg != "0" ~ "hook/trichuris",
                                  trichuris_egg != "0" & ascaris_egg != "0" ~ "trichuris/ascaris",
                         ascaris_egg != "0" & Specie == "Strongyloids"~ "ascaris/strongyloids",
                         trichuris_egg != "0" & Specie == "Strongyloids"~ "trichuris/strongyloids",
                         hookworm_egg != "0" & Specie %in% c("Strongyloids", "Hook//strong") ~ "hook/strongyloids"))


data_A <- FLORA_data %>%
  filter(random == "A")

data_B <- FLORA_data %>%
  filter(random == "B")
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
all <- FLORA_data %>%
  count(random)

#########
age_A <- summary(data_A$age)
age_B <- summary(data_B$age)
#########
sex <- FLORA_data %>%
  count(random, gender)

#########
bmi_A <- summary(data_A$bmi)
bmi_B <- summary(data_B$bmi)

#########
systol_A <- summary(data_A$systolic)
systol_B <- summary(data_B$systolic)

#########
hook <- FLORA_data %>%
  filter(hookworm_egg != "0") %>%
  count(random)

ascaris <- FLORA_data %>%
  filter(ascaris_egg != "0") %>%
  count(random)

trichuris <- FLORA_data %>%
  filter(trichuris_egg != "0") %>%
  count(random)

specieS <- FLORA_data %>%
  filter(str_detect(Specie, "trong"))  %>%
  count(random)

coinf_HTA <- FLORA_data %>%
  filter(co_infection == "hook/trichuris/ascaris") %>%
  count(random)

coinf_HTS <- FLORA_data %>%
  filter(co_infection == "hook/trichuris/strongyloids") %>%
  count(random)

coinf_HT <- FLORA_data %>%
  filter(co_infection == "hook/trichuris") %>%
  count(random)

coinf_HA <- FLORA_data %>%
  filter(co_infection == "hook/ascaris")  %>%
  count(random)

coinf_TA <- FLORA_data %>%
  filter(co_infection == "trichuris/ascaris")  %>%
  count(random)

coinf_HS <- FLORA_data %>%
  filter(co_infection == "hook/strongyloids") %>%
  count(random)

```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_0 <- tibble(Variable = c("Number of participants, n",
                             "Age median (range)", "Gender Female/Male", "BMI, Mean",
                             "Systolic Blood Pressure, Mean", "Hookworm", "Trichuris trichiura",
                             "Ascaris lumbricoides",  "Strongyloides", "Hookworm/Trichuris/Ascaris",
                             "Hookworm/Trichuris/Strongyloides", "Hookworm/Trichuris",
                             "Hookworm/Strongyloides"),
                Intervention = c(all$n[1],
                                 paste0(age_A[3], "(", age_A[1], "-", age_A[6], ")"),
                                 paste0(sex$n[1], "/", sex$n[2]), round(bmi_A[4], 1),
                                 round(systol_A[4]), hook$n[1], trichuris$n[1], ascaris$n[1],
                                 specieS$n[1], coinf_HTA$n[1], coinf_HTS$n[1],
                                 coinf_HT$n[1], coinf_HS$n[1]),
                Placebo = c(all$n[2],
                            paste0(age_B[3], "(", age_B[1], "-", age_B[6], ")"),
                            paste0(sex$n[3], "/", sex$n[4]), round(bmi_B[4], 1),
                            round(systol_B[4]), hook$n[2], trichuris$n[2], ascaris$n[2],
                            specieS$n[2], coinf_HTA$n[2], 0, coinf_HT$n[2], coinf_HS$n[2]))
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
kable(tab_0, booktabs = T, caption = "Baseline Characteristics of Participants", linesep = " ",
      col.names = c(" ", "Intervention", "Placebo"),
      format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("hold_position"), font_size = 9) %>%
  row_spec(row = 0, bold = TRUE)%>%
  pack_rows("Demographics", 1, 5)%>%
  pack_rows("Helminth Infection", 6, 9)%>%
  pack_rows("Co-infection", 10, 13)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
data_10 <- FloraAE %>%
  filter(days == "10 days after")

symt_10 <- data_10 %>%
  count(Treatment_arm, Reaction)

particip_10 <- data_10 %>%
  select(Participant_ID, Treatment_arm, Enrolment_date)%>%
  unique()%>%
  count(Treatment_arm)

reaction10 <- data_10 %>%
  select(Participant_ID, Treatment_arm, Reaction)%>%
  unique()%>%
  count(Treatment_arm, Reaction)
###################

data_30 <- FloraAE %>%
  filter(days == "1 month after")

symt_30 <- data_30 %>%
  count(Treatment_arm, Reaction)

particip_30 <- data_30 %>%
  select(Participant_ID, Treatment_arm, Enrolment_date)%>%
  unique()%>%
  count(Treatment_arm)

reaction30 <- data_30 %>%
  select(Participant_ID, Treatment_arm, Reaction)%>%
  unique()%>%
  count(Treatment_arm, Reaction)
###################

data_90 <- FloraAE %>%
  filter(days == "3 months after")

symt_90 <- data_90 %>%
  count(Treatment_arm, Reaction)

particip_90 <- data_90 %>%
  select(Participant_ID, Treatment_arm, Enrolment_date)%>%
  unique()%>%
  count(Treatment_arm)

reaction90 <- data_90 %>%
  select(Participant_ID, Treatment_arm, Reaction)%>%
  unique()%>%
  count(Treatment_arm, Reaction)
##################

data_180 <- FloraAE %>%
  filter(days == "6 months after")

symt_180 <- data_180 %>%
  count(Treatment_arm, Reaction)

particip_180 <- data_180 %>%
  select(Participant_ID, Treatment_arm, Enrolment_date)%>%
  unique()%>%
  count(Treatment_arm)

reaction180 <- data_180 %>%
  select(Participant_ID, Treatment_arm, Reaction)%>%
  unique()%>%
  count(Treatment_arm, Reaction)
###################

all_adv <- FloraAE %>%
  count(Treatment_arm, Reaction)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_1 <- tibble(`Number of` = c("Adverse Events (Solicited/Unsolicited)", "Participants",
                                "Participants (Solicited/Unsolicited)",
                                "Adverse Events (Solicited/Unsolicited)", "Participants ",
                                "Participants (Solicited/Unsolicited)",
                                "Adverse Events (Solicited/Unsolicited)", "Participants",
                                "Participants (Solicited/Unsolicited)",
                                "Adverse Events (Solicited/Unsolicited)", "Participants",
                                "Participants (Solicited/Unsolicited)",
                                "N (Solicited/Unsolicited)",
                                "Participants that experienced an AE"),
                Intervention = c(paste0(symt_10$n[1], "/", symt_10$n[2]), particip_10$n[1],
                                 paste0(reaction10$n[1], "/", reaction10$n[2]),
                                 paste0(symt_30$n[1], "/", symt_30$n[2]), particip_30$n[1],
                                 paste0(reaction30$n[1], "/", reaction30$n[2]),
                                 paste0(symt_90$n[1], "/", symt_90$n[2]), particip_90$n[1],
                                 paste0(reaction90$n[1], "/", reaction90$n[2]),
                                 paste0(symt_180$n[1], "/", symt_180$n[2]), particip_180$n[1],
                                 paste0(reaction180$n[1], "/", reaction180$n[2]),
                                 paste0(all_adv$n[1], "/", all_adv$n[2]), all$n[1]),
                Placebo = c(paste0(symt_10$n[3], "/", symt_10$n[4]), particip_10$n[2],
                            paste0(reaction10$n[3], "/", reaction10$n[4]),
                            paste0(symt_30$n[3], "/", symt_30$n[4]), particip_30$n[2],
                            paste0(reaction30$n[3], "/", reaction30$n[4]),
                            paste0(symt_90$n[3], "/", symt_90$n[4]), particip_90$n[2],
                            paste0(reaction90$n[3], "/", reaction90$n[4]),
                            paste0(symt_180$n[3], "/", symt_180$n[4]), particip_180$n[2],
                            paste0(reaction180$n[3], "/", reaction180$n[4]),
                            paste0(all_adv$n[3], "/", all_adv$n[4]), all$n[2]),
                Total = c(sum(symt_10$n), sum(particip_10$n), NA, sum(symt_30$n),
                          sum(particip_30$n), NA, sum(symt_90$n), sum(particip_90$n), NA,
                          sum(symt_180$n), sum(particip_180$n), NA,
                          sum(all_adv$n), sum(all$n)))
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
kable(tab_1, booktabs = T,linesep = " ",
      caption = "Number of Adverse Events and Number of Participants Reporting Adverse Events",
      format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("hold_position"), font_size = 8.5) %>%
  row_spec(row = 0, bold = TRUE)%>%
  pack_rows(index = c("1-10 days after enrollment" = 3,
                      "11 days - 1 month after enrollment" = 3,
                      "1-3 months after enrollment" = 3,
                      "3-6 months after enrollment" = 3,
                      "Total number of adverse events" = 2))
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
# tab_2 <- tab_1 %>%
#   mutate(`Time Point` = c("1-10 days after enrollment", " "," ",
#                   "11 days - 1 month after enrollment", " ", " ",
#                   "1-3 months after enrollment", " ", " ",
#                   "3-6 months after enrollment", " ", " ",
#                   "Total number of adverse events", " ")) %>%
#   relocate(`Time Point`, .before = `Number of`)
# 
# 
# kable(tab_2, booktabs = T,linesep = " ",
#       caption = "Number of Adverse Events and Number of Participants Reporting Adverse Events",
#       format.args = list(big.mark = ' ')) %>%
#   kable_styling(latex_options = c("hold_position"), font_size = 8) %>%
#   row_spec(row = 0, bold = TRUE)
```

\newpage
\center __Repository__ \center

- Date of report: `r format(Sys.Date(), "%Y-%m-%d")`
- Analysis performed by: Saïdou MAHMOUDOU
- Code repository: [gitlab.com/Saidou](https://gitlab.com/Saidou/flora_publish.git)
- Local storage path: NAS/Saidou.M